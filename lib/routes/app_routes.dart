import 'package:auto_route/annotations.dart';
import 'package:kiki_mobile/layouts/authentication_layout.dart';
import 'package:kiki_mobile/routes/routes_paths.dart';
import 'package:kiki_mobile/screens/home/home_page_screen.dart';
import 'package:kiki_mobile/screens/login/login_screen.dart';
import 'package:kiki_mobile/screens/register/register_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: HomeScreen, initial: true),
    AutoRoute(
        path: RoutePaths.authentication,
        page: AuthenticationLayout,
        children: [
          AutoRoute(
              path: RoutePaths.authenticationLogin,
              page: LoginScreen,
              initial: true),
          AutoRoute(
              path: RoutePaths.authenticationRegister, page: RegisterScreen)
        ]),
  ],
)
class $AppRouter {}
