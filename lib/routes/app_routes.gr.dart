// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i5;
import 'package:flutter/material.dart' as _i6;

import '../layouts/authentication_layout.dart' as _i2;
import '../screens/home/home_page_screen.dart' as _i1;
import '../screens/login/login_screen.dart' as _i3;
import '../screens/register/register_screen.dart' as _i4;

class AppRouter extends _i5.RootStackRouter {
  AppRouter([_i6.GlobalKey<_i6.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i5.PageFactory> pagesMap = {
    HomeScreen.name: (routeData) {
      final args = routeData.argsAs<HomeScreenArgs>(
          orElse: () => const HomeScreenArgs());
      return _i5.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i1.HomeScreen(key: args.key),
      );
    },
    AuthenticationLayout.name: (routeData) {
      return _i5.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i2.AuthenticationLayout(),
      );
    },
    LoginScreen.name: (routeData) {
      return _i5.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i3.LoginScreen(),
      );
    },
    RegisterScreen.name: (routeData) {
      return _i5.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i4.RegisterScreen(),
      );
    },
  };

  @override
  List<_i5.RouteConfig> get routes => [
        _i5.RouteConfig(
          HomeScreen.name,
          path: '/',
        ),
        _i5.RouteConfig(
          AuthenticationLayout.name,
          path: '/auth',
          children: [
            _i5.RouteConfig(
              '#redirect',
              path: '',
              parent: AuthenticationLayout.name,
              redirectTo: 'login',
              fullMatch: true,
            ),
            _i5.RouteConfig(
              LoginScreen.name,
              path: 'login',
              parent: AuthenticationLayout.name,
            ),
            _i5.RouteConfig(
              RegisterScreen.name,
              path: 'register',
              parent: AuthenticationLayout.name,
            ),
          ],
        ),
      ];
}

/// generated route for
/// [_i1.HomeScreen]
class HomeScreen extends _i5.PageRouteInfo<HomeScreenArgs> {
  HomeScreen({_i6.Key? key})
      : super(
          HomeScreen.name,
          path: '/',
          args: HomeScreenArgs(key: key),
        );

  static const String name = 'HomeScreen';
}

class HomeScreenArgs {
  const HomeScreenArgs({this.key});

  final _i6.Key? key;

  @override
  String toString() {
    return 'HomeScreenArgs{key: $key}';
  }
}

/// generated route for
/// [_i2.AuthenticationLayout]
class AuthenticationLayout extends _i5.PageRouteInfo<void> {
  const AuthenticationLayout({List<_i5.PageRouteInfo>? children})
      : super(
          AuthenticationLayout.name,
          path: '/auth',
          initialChildren: children,
        );

  static const String name = 'AuthenticationLayout';
}

/// generated route for
/// [_i3.LoginScreen]
class LoginScreen extends _i5.PageRouteInfo<void> {
  const LoginScreen()
      : super(
          LoginScreen.name,
          path: 'login',
        );

  static const String name = 'LoginScreen';
}

/// generated route for
/// [_i4.RegisterScreen]
class RegisterScreen extends _i5.PageRouteInfo<void> {
  const RegisterScreen()
      : super(
          RegisterScreen.name,
          path: 'register',
        );

  static const String name = 'RegisterScreen';
}
