String getPath({String prefix = '', required String path}) {
  return '$prefix/$path';
}

class RoutePaths {
  static const String authentication = '/auth';
  static const String authenticationLogin = 'login';
  static String authenticationLoginPath = getPath(
      prefix: RoutePaths.authentication, path: RoutePaths.authenticationLogin);
  static const String authenticationRegister = 'register';
}
