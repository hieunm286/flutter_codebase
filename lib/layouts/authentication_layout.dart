import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class AuthenticationLayout extends StatelessWidget {
  const AuthenticationLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: AutoRouter(),
      ),
    );
  }
}
