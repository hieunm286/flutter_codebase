import 'package:flutter/material.dart';

class TextFormFieldTheme {
  // private constructor
  TextFormFieldTheme._();

  static InputDecorationTheme lightInputDecorationTheme =
      const InputDecorationTheme(
    border: OutlineInputBorder(),
    contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 0),
  );

  static InputDecorationTheme darkInputDecorationTheme =
      const InputDecorationTheme(
    border: OutlineInputBorder(),
    contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 0),
  );
}
