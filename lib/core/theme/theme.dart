import 'package:flutter/material.dart';
import 'package:kiki_mobile/core/constants/colors.dart';
import 'package:kiki_mobile/core/theme/text_form_field.dart';

class AppTheme {
  static ThemeData lightTheme = ThemeData(
      brightness: Brightness.light,
      primarySwatch: const MaterialColor(0xff204DC2, <int, Color>{
        50: ColorsPalette.primaryColor,
        100: ColorsPalette.primaryColor,
        200: ColorsPalette.primaryColor,
        300: ColorsPalette.primaryColor,
        400: ColorsPalette.primaryColor,
        500: ColorsPalette.primaryColor,
        600: ColorsPalette.primaryColor,
        700: ColorsPalette.primaryColor,
        800: ColorsPalette.primaryColor,
        900: ColorsPalette.primaryColor
      }),
      inputDecorationTheme: TextFormFieldTheme.lightInputDecorationTheme);
  
  static ThemeData darkTheme = ThemeData(
      brightness: Brightness.dark,
      inputDecorationTheme: TextFormFieldTheme.lightInputDecorationTheme);
}
