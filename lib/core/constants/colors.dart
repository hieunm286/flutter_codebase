import 'package:flutter/material.dart';

class ColorsPalette {
  static const Color primaryColor = Color(0xff204DC2);
  static const Color secondaryColor = Color(0xff212B36);
  static const Color subColor = Color(0xff8B9299);
}
