const double defaultPadding = 16.0;
const double mediumPadding = 24.0;
const double smallPadding = 8.0;
const int defaultBorderRadius = 4;
