import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en') +
      {
        'en': {
          'Nhập email': 'Enter email',
          'Đăng nhập': 'Login',
          'Login': 'Đăng nhập tiếng anh',
          'Tiếng việt': 'Vietnamese',
          'Tiếng anh': 'English'
        },
        'vi': {
          'Nhập email': 'Nhập email',
          'Đăng nhập': 'Đăng nhập',
          'Login': 'Đăng nhập tiếng việt',
          'Tiếng việt': 'Tiếng việt',
          'Tiếng anh': 'Tiếng anh'
        },
      };

  String get i18n => localize(this, _t);

  String fill(List<Object> params) => localizeFill(this, params);
}
