import 'package:flutter/material.dart';
import 'package:kiki_mobile/core/constants/dimensions.dart';
import 'package:kiki_mobile/core/extensions/i18n.dart';
import 'package:kiki_mobile/core/extensions/text_style.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Email'),
            SizedBox(height: smallPadding),
            TextFormField(
              decoration: InputDecoration(
                hintText: 'Nhập email'.i18n,
              ),
            ),
            SizedBox(height: defaultPadding),
            Text('Mật khẩu'),
            SizedBox(height: smallPadding),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Nhập mật khẩu',
                  suffixIcon: Icon(Icons.visibility)),
            ),
            SizedBox(height: defaultPadding),
            Container(
              width: double.infinity,
              child: Text(
                'Quên mật khẩu',
                style: TextStyles.defaultStyle.primaryTextColor,
                textAlign: TextAlign.right,
              ),
            ),
            SizedBox(
              height: smallPadding * 1.5,
            ),
            Container(
                width: double.infinity,
                child:
                    ElevatedButton(onPressed: () {}, child: Text('Login'.i18n)))
          ],
        ),
      ),
    );
  }
}
