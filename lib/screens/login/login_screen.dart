import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:kiki_mobile/core/constants/dimensions.dart';
import 'package:kiki_mobile/core/extensions/i18n.dart';
import 'package:kiki_mobile/screens/login/components/login_form.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    String language = I18n.language;
    print(language);

    void changeLanguage(String language) {
      I18n.of(context).locale = Locale(language);
    }

    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: mediumPadding),
          child: Column(
            children: [
              const LoginForm(),
              const SizedBox(height: mediumPadding * 2),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: Container(
                          margin: const EdgeInsets.only(right: defaultPadding),
                          child: const Divider())),
                  const Text('Hoặc'),
                  Expanded(
                      child: Container(
                          margin: const EdgeInsets.only(left: defaultPadding),
                          child: const Divider())),
                ],
              ),
              const SizedBox(height: mediumPadding * 2),
              ElevatedButton(
                  onPressed: () {
                    changeLanguage('vi');
                  },
                  child: Text('Tiếng việt'.i18n)),
              ElevatedButton(
                  onPressed: () {
                    changeLanguage('en');
                  },
                  child: Text('Tiếng anh'.i18n))
            ],
          ),
        ),
      ),
    );
  }
}
