import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:kiki_mobile/core/utils/local_storage.dart';
import 'package:kiki_mobile/routes/routes_paths.dart';
import 'package:kiki_mobile/screens/home/introduction_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);
  final ignoreIntroScreen =
      LocalStorageHelper.getValue('ignoreIntroScreen') as bool?;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkShowIntroScreen();
  }

  void checkShowIntroScreen() async {
    if (!mounted) return;
    if (widget.ignoreIntroScreen == null) {
      LocalStorageHelper.setValue('ignoreIntroScreen', true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.ignoreIntroScreen == null
        ? const IntroductionScreen()
        : Scaffold(
            body: SafeArea(
              child: Column(
                children: [
                  Text('Home screen'),
                  ElevatedButton(
                      onPressed: () {
                        context.router
                            .pushNamed(RoutePaths.authenticationLoginPath);
                      },
                      child: Text('Login now'))
                ],
              ),
            ),
          );
  }
}
